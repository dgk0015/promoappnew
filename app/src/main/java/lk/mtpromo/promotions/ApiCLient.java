package lk.mtpromo.promotions;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiCLient {

    private static final String BASE_URL = "http://activations2.emarketer.lk/api/v1/";
    private static ApiCLient mInstance;
    private static Retrofit retrofit = null;

    private final static String ACCESS_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNDE3Y2UyYjExOTdiYjkyYzZhNjYwMGNlNzY1YWZkOTVjZjY3MGI1ODM0MGY2YTdmZTU4MmUzNmFiNmJmN2JmOGVmMjgyYWUzYTdhN2VhY2EiLCJpYXQiOjE1ODEzNzE0OTAsIm5iZiI6MTU4MTM3MTQ5MCwiZXhwIjoxNjEyOTkzODkwLCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.DZVVQH72iJEV1xMCO3LUaAp5VIkcYTBpEGzjrw1QY4pIobM5IKPEdrKiQA3NfpsqPQAshE7EVrj44Q8JGsOiaL90brWl_RLysYCdDKzx7rhim-H6Kknc4CuPkkDrzMgHw7Ogo7YvCBfVQX9GUI5q75NJogpLupO_4000OVRbnxXHqrzuDS3wk8I9CEdHOTrEJYIpoHGPAmTJz33k7F74u8R1KIabIBGCLLdlh9VEqxyE_B-7kC0JIp3AkW2GRhIW58o41rM7wlT6L7vWfw7KeaJGLxPq0hW9ZItl1yQ5MCtFiQlqk9veHpJwyUzD_R84Ke9ZdSaEL0UyJ-r__7ac1LbTJ0wBFDcHZhvhzRKfp94g74RKTi-pZwoikAFkTSAEHtjVN1KGmyUt7UeiH6wk39b8EsRNVgkV57woxwrO8hEcdH5AgIzQeA3JwevH3kF-5kikFLHUBqIyAoGOFBiA9hrk4NEG2tfLsNyP_rXHYRRpM5mU3sfjrF6Cww9a3cvzJDwGBkZxaa8MsjqYKOK0a2ekLwypDbdrjtggpA2xUtBCf71R5uyOwdaEXC8bL1VHhY8GvofA4dty1WYln_hAawEqBthi20B9VWUNgIaMIC9311YQpwxdh1PzpaeM4tIsOxTph8OLtmJ-nDUsI7v8GxvPLfTUCOy-K_3QPo8nsus";

    OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request newRequest  = chain.request().newBuilder()
                    .addHeader("Authorization", "Bearer " + ACCESS_TOKEN)
                    .build();
            return chain.proceed(newRequest);
        }
    }).build();

    public Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .client(client)
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

//    Retrofit retrofit = new Retrofit.Builder()
//            .client(client)
//            .baseUrl(BASE_URL)
//            .addConverterFactory(GsonConverterFactory.create())
//            .build();
}
