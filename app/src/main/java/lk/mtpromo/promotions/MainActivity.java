package lk.mtpromo.promotions;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnSuccessListener;

import lk.mtpromo.promotions.Model.ProfileInformationResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private Float floatEnter;
    private EditText editTextName;
    private EditText editTextMobileNumber;
    private EditText editTextNIC;
    private Spinner spinnerPromotion;
    private Spinner spinnerChannel;
    private Spinner spinnerOutlet;
    private String name;
    private String mobile_number;
    private String nic;
    private String device_id;
    private String userId;
    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

    private static final int REQUEST_CHECK_SETTINGS = 100;

    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;

    // boolean flag to toggle the ui
    private Boolean mRequestingLocationUpdates;

    private FusedLocationProviderClient fusedLocationClient;

    public static final String SHARED_PREFS = "sharedPrefs";
//    public static int userid;
    public static String NIC = "nic";
    public static String USERID = "userid";

//    LocationTrack locationTrack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
//        SharedPreferences.Editor myEditor = myPreferences.edit();
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String userid = sharedPreferences.getString(USERID, "");

        if (userid != "") {
            Intent intent = new Intent(MainActivity.this, Main2Activity.class);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            startActivity(intent);
            finish();
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        System.out.println("location" +location);
                        if (location != null) {
                            // Logic to handle location object
                        }
                    }
                });

        initViews();
        getDeviceId();
        initLocation();
    }

    private void initLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
//                System.out.println("Current Location" +mCurrentLocation.toString() );
            }
        };
    }

    private String getDeviceId() {
        String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        return android_id;
    }

    private Context getContext() {
        return null;
    }

    private void initViews() {
        editTextName = findViewById(R.id.edtName);
        editTextMobileNumber = findViewById(R.id.edtMobileNumber);
        editTextNIC = findViewById(R.id.edtNIC);
    }

    public void onClickEnter (View view) {
        name = editTextName.getText().toString().trim();
        mobile_number = editTextMobileNumber.getText().toString().trim();
        nic = editTextNIC.getText().toString().trim();
        device_id = getDeviceId();
        if(!TextUtils.isEmpty(name) && !TextUtils.isEmpty(mobile_number) && !TextUtils.isEmpty(nic)) {
            SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(NIC, nic);
            editor.apply();
            createUser(name, mobile_number, nic, device_id);
        }
    }

    private void createUser(String name, String mobile_number, String nic, String device_id){

        ApiCLient apiCLient = new ApiCLient();
        ApiInterface apiInterface = apiCLient.getClient().create(ApiInterface.class);

        apiInterface.createUser(name, mobile_number, nic, device_id)
        .enqueue(new Callback<ProfileInformationResponse>() {
            @Override
            public void onResponse(Call<ProfileInformationResponse> call, Response<ProfileInformationResponse> response) {
                if (response.body().getSuccess()) {
                    Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                    SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    editor.putString(USERID, response.body().getData().getId());
                    editor.commit();
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(MainActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ProfileInformationResponse> call, Throwable t) {

            }
        });
    }

}
