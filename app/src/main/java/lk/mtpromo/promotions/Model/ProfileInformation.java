package lk.mtpromo.promotions.Model;

import com.google.gson.annotations.SerializedName;

public class ProfileInformation {

    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("nic")
    private String nic;
    @SerializedName("mobile_number")
    private String mobileNumber;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("id")
    private String id;

    public ProfileInformation(
            String name, String mobileNumber, String nic, String email, String updatedAt, String createdAt, String id
    ) {
        this.name = name;
        this.mobileNumber = mobileNumber;
        this.nic = nic;
        this.email = email;
        this.updatedAt = updatedAt;
        this.createdAt = createdAt;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String data) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Post{" +
                "name='" + name + '\'' +
                ", mobile_number='" + mobileNumber + '\'' +
                ", nic=" + nic +
                '}';
    }
}
