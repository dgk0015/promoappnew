package lk.mtpromo.promotions.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FreeItemsListResponse {

    @SerializedName("success")
    private Boolean success;
    @SerializedName("message")
    private String message;
    @SerializedName("result")
    private List<Items> results;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Items> getResults() {
        return results;
    }

    public void setResults(List<Items> results) {
        this.results = results;
    }

}
