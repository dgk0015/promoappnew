package lk.mtpromo.promotions.Model;

import com.google.gson.annotations.SerializedName;

public class ProfileInformationResponse {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private ProfileInformation data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ProfileInformation getData() {
        return data;
    }

    public void setData(ProfileInformation data) {
        this.data = data;
    }
}
