package lk.mtpromo.promotions.Model;

import com.google.gson.annotations.SerializedName;

public class ItemUpdate {
    @SerializedName("id")
    private String id;
    @SerializedName("campaign_id")
    private String campaignId;
    @SerializedName("transaction_id")
    private String transactionId;
    @SerializedName("promotion_id")
    private String promotionId;
    @SerializedName("item_id")
    private String itemId;
    @SerializedName("target_qty")
    private String targetQty;
    @SerializedName("pause_qty")
    private String pauseQty;
    @SerializedName("end_qty")
    private String endQty;
    @SerializedName("isfee")
    private String isfee;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("deleted_at")
    private String deletedAt;

    public ItemUpdate(
            String id, String campaignId, String transactionId, String promotionId, String itemId,
            String targetQty, String pauseQty, String endQty, String isfee, String createdAt,
            String updatedAt, String deletedAt
    ) {
        this.id = id;
        this.campaignId = campaignId;
        this.transactionId = transactionId;
        this.promotionId = promotionId;
        this.itemId = itemId;
        this.targetQty = targetQty;
        this.pauseQty = pauseQty;
        this.endQty = endQty;
        this.isfee = isfee;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getTargetQty() {
        return targetQty;
    }

    public void setTargetQty(String targetQty) {
        this.targetQty = targetQty;
    }

    public String getPauseQty() {
        return pauseQty;
    }

    public void setPauseQty(String pauseQty) {
        this.pauseQty = pauseQty;
    }

    public String getEndQty() {
        return endQty;
    }

    public void setEndQty(String endQty) {
        this.endQty = endQty;
    }

    public String getIsfee() {
        return isfee;
    }

    public void setIsfee(String isfee) {
        this.isfee = isfee;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    @Override
    public String toString() {
        return "Post{" +
                "item_id='" + itemId + '\'' +
                ", pause_qty='" + pauseQty +
                '}';
    }
}
