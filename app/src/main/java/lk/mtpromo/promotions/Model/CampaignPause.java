package lk.mtpromo.promotions.Model;

import com.google.gson.annotations.SerializedName;

public class CampaignPause {

    @SerializedName("lat")
    private String lat;
    @SerializedName("lang")
    private String lang;
    @SerializedName("time")
    private String time;

    public CampaignPause(
            String lat, String lang, String time
    ) {
        this.lat = lat;
        this.lang = lang;
        this.time = time;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Post{" +
                "lat='" + lat + '\'' +
                ", lang='" + lang + '\'' +
                ", time='" + time + '\'' +
                '}';
    }

}
