package lk.mtpromo.promotions.Model;

import com.google.gson.annotations.SerializedName;

public class ItemUpdateResponse {

    @SerializedName("success")
    private Boolean success;
    @SerializedName("message")
    private String message;
    @SerializedName("result")
    private ItemUpdate results;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ItemUpdate getResults() {
        return results;
    }

    public void setResults(ItemUpdate results) {
        this.results = results;
    }

}
