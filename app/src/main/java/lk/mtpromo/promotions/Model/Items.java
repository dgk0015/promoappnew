package lk.mtpromo.promotions.Model;

import com.google.gson.annotations.SerializedName;

public class Items {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("target_qty")
    private String targetQty;
    @SerializedName("pause_qty")
    private String pauseQty;
    @SerializedName("end_qty")
    private String endQty;

    public Items(
            String id, String name, String targetQty, String pauseQty, String endQty
    ) {
        this.id = id;
        this.name = name;
        this.targetQty = targetQty;
        this.pauseQty = pauseQty;
        this.endQty = endQty;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTargetQty() {
        return targetQty;
    }

    public void setTargetQty(String targetQty) {
        this.targetQty = targetQty;
    }

    public String getPauseQty() {
        return pauseQty;
    }

    public void setPauseQty(String pauseQty) {
        this.pauseQty = pauseQty;
    }

    public String getEndQty() {
        return endQty;
    }

    public void setEndQty(String endQty) {
        this.endQty = endQty;
    }
}
