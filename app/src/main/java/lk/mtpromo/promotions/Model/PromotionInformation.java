package lk.mtpromo.promotions.Model;

import com.google.gson.annotations.SerializedName;

public class PromotionInformation {

    @SerializedName("lat")
    private String lat;
    @SerializedName("lang")
    private String lang;
    @SerializedName("promotion_id")
    private String promotionId;
    @SerializedName("transaction_id")
    private String transactionId;
    @SerializedName("channel_id")
    private String channelId;
    @SerializedName("outlet_id")
    private String outletId;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("id")
    private String id;
    @SerializedName("image")
    private String image;


    public PromotionInformation(
            String lat, String lang, String promotionId, String transactionId, String channelId, String outletId, String userId, String updatedAt, String createdAt, String id, String image
    ) {
        this.lat = lat;
        this.lang = lang;
        this.promotionId = promotionId;
        this.transactionId = transactionId;
        this.channelId = channelId;
        this.outletId = outletId;
        this.userId = userId;
        this.updatedAt = updatedAt;
        this.createdAt = createdAt;
        this.id = id;
        this.image = image;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.promotionId = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Post{" +
                "lat='" + lat + '\'' +
                ", lang='" + lang + '\'' +
                ", promotionId='" + promotionId + '\'' +
                ", channelId='" + channelId + '\'' +
                ", outletId='" + outletId + '\'' +
                ", nic=" + userId + '\'' +
                ", image=" + image +
                '}';
    }

}
