package lk.mtpromo.promotions.Model;

import com.google.gson.annotations.SerializedName;

public class CampaignPauseResponse {

        @SerializedName("success")
        private Boolean success;
        @SerializedName("message")
        private String message;
        @SerializedName("result")
        private CampaignPause result;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public CampaignPause getResult() {
            return result;
        }

        public void setData(CampaignPause result) {
            this.result = result;
        }
}
