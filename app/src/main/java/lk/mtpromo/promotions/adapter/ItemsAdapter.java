package lk.mtpromo.promotions.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import lk.mtpromo.promotions.Model.Items;
import lk.mtpromo.promotions.Model.ItemsListResponse;
import lk.mtpromo.promotions.R;
import retrofit2.Callback;

//import info.androidhive.retrofit.R;
//import info.androidhive.retrofit.model.Movie;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ItemsViewHolder> {

    private List<Items> items;
    private int rowLayout;
    private Context context;
    private OnItemClickListener mOnItemClickListener;

    public static class ItemsViewHolder extends RecyclerView.ViewHolder {
        LinearLayout moviesLayout;
        TextView itemTitle;
        TextView targetQty;
        TextView completeQty;
        TextView movieDescription;
        View lyt_parent;


        public ItemsViewHolder(View v) {
            super(v);
            moviesLayout = (LinearLayout) v.findViewById(R.id.movies_layout);
            itemTitle = (TextView) v.findViewById(R.id.title);
            targetQty = (TextView) v.findViewById(R.id.targetQty);
            completeQty = (TextView) v.findViewById(R.id.completeQty);
//            movieDescription = (TextView) v.findViewById(R.id.description);
            lyt_parent = (View) v.findViewById(R.id.movies_layout);        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Items obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public ItemsAdapter(List<Items> items, int rowLayout, Callback<ItemsListResponse> context) {
        this.items = items;
        this.rowLayout = rowLayout;
    }

    @Override
    public ItemsAdapter.ItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        RecyclerView.ViewHolder vh;
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ItemsViewHolder(view);
//        vh = new ItemsViewHolder(view);
//        return vh;
    }


    @Override
    public void onBindViewHolder(ItemsViewHolder holder, final int position) {
        ItemsViewHolder view = (ItemsViewHolder) holder;
        holder.itemTitle.setText(items.get(position).getName());
        holder.targetQty.setText(items.get(position).getTargetQty());
        holder.completeQty.setText(items.get(position).getPauseQty());
//        holder.data.setText(movies.get(position).getReleaseDate());
//        holder.movieDescription.setText(movies.get(position).getOverview());
//        holder.rating.setText(movies.get(position).getVoteAverage().toString());

        Items p = items.get(position);
//        view.name.setText(p.name);
//        Tools.displayImageRound(ctx, view.image, p.image);
        view.lyt_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view, items.get(position), position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}