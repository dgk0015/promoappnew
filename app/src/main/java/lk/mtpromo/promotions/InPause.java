package lk.mtpromo.promotions;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import lk.mtpromo.promotions.Model.CampaignPauseResponse;
import lk.mtpromo.promotions.Model.ItemUpdateResponse;
import lk.mtpromo.promotions.Model.Items;
import lk.mtpromo.promotions.Model.ItemsListResponse;
import lk.mtpromo.promotions.adapter.ItemsAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InPause extends AppCompatActivity {

    private ItemsAdapter mAdapter;
    private Button buttonResume;
    private String campaignId;
    private String transactionId;
    private String lat;
    private String lang;

    LocationTrack locationTrack;

    public static final String SHARED_PREFS = "sharedPrefs";
    public static String CAMPAIGNID = "campaignId";
    public static String TRANSACTIONID = "tansactionId";
    public static String USERID = "userid";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_pause);

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        campaignId = sharedPreferences.getString(CAMPAIGNID, "");
        transactionId = sharedPreferences.getString(TRANSACTIONID, "");

        locationTrack = new LocationTrack(InPause.this);

        if (locationTrack.canGetLocation( )) {

            lang = Double.toString(locationTrack.getLongitude());
            lat = Double.toString(locationTrack.getLatitude());

        } else {

            locationTrack.showSettingsAlert();
        }

        String pattern3 = "yyyy-MM-dd hh:mm";
        SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat(pattern3);
        final String datetime = simpleDateFormat3.format(new Date());

        buttonResume = (Button) findViewById(R.id.btnResume);

        buttonResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiCLient apiCLient = new ApiCLient();
                ApiInterface apiInterface = apiCLient.getClient().create(ApiInterface.class);

                apiInterface.endCampaign(transactionId, lang, lat, datetime)
                        .enqueue(new Callback<CampaignPauseResponse>() {
                            @Override
                            public void onResponse(Call<CampaignPauseResponse> call, Response<CampaignPauseResponse> response) {
                                if (response.body().getSuccess()) {
                                    Intent intent = new Intent(InPause.this, InProgress.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(InPause.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<CampaignPauseResponse> call, Throwable t) {

                            }
                        });
            }
        });

        getGeneralItemsList();
        getFreeItemsList();
    }

    /** get general items list */
    private void getGeneralItemsList() {
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.movies_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        ApiCLient apiCLient = new ApiCLient();
        ApiInterface apiInterface = apiCLient.getClient().create(ApiInterface.class);

        /** get items list */
        Call<ItemsListResponse> call = apiInterface.getItems(campaignId);
        call.enqueue(new Callback<ItemsListResponse>() {
            @Override
            public void onResponse(Call<ItemsListResponse> call, Response<ItemsListResponse> response) {
                List<Items> items = response.body().getResults();
                mAdapter = new ItemsAdapter( items, R.layout.list_items, this );
                recyclerView.setAdapter(mAdapter);

                mAdapter.setOnItemClickListener(new ItemsAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, Items obj, int position) {
                        showCustomDialog(obj);
                    }
                });
//                showCustomDialog(items.get(0));
                Log.d("message",items.toString());
            }

            private void showCustomDialog(final Items items) {
                final Dialog dialog = new Dialog(InPause.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
                dialog.setContentView(R.layout.dialog_light);
                dialog.setCancelable(true);

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

                ((TextView) dialog.findViewById(R.id.title)).setText(items.getName());
                ((TextView) dialog.findViewById(R.id.targetQty)).setText(items.getTargetQty());
                ((TextView) dialog.findViewById(R.id.completeQty)).setText(items.getPauseQty());

                ((Button) dialog.findViewById(R.id.dlgCancel)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                ((Button) dialog.findViewById(R.id.dlgDone)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final String itemId = items.getId();
                        final String qtyComplete = ((EditText) dialog.findViewById(R.id.edtQty)).getText().toString().trim();

                        ApiCLient apiCLient = new ApiCLient();
                        ApiInterface apiInterface = apiCLient.getClient().create(ApiInterface.class);

                        apiInterface.pauseItem(campaignId, itemId, qtyComplete)
                                .enqueue(new Callback<ItemUpdateResponse>() {
                                    @Override
                                    public void onResponse(Call<ItemUpdateResponse> call, Response<ItemUpdateResponse> response) {
                                        if (response.body().getSuccess()) {
                                            getGeneralItemsList();
                                            dialog.dismiss();
                                        } else {
                                            Toast.makeText(InPause.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ItemUpdateResponse> call, Throwable t) {

                                    }
                                });
                    }
                });

                dialog.show();
                dialog.getWindow().setAttributes(lp);
            }

            @Override
            public void onFailure(Call<ItemsListResponse> call, Throwable t) {

            }
        });
    }

    /** get free items list */
    private void getFreeItemsList() {
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.movies_recycler_view2);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        ApiCLient apiCLient = new ApiCLient();
        ApiInterface apiInterface = apiCLient.getClient().create(ApiInterface.class);

        /** get items list */
        Call<ItemsListResponse> call = apiInterface.getItemsFree(campaignId);
        call.enqueue(new Callback<ItemsListResponse>() {
            @Override
            public void onResponse(Call<ItemsListResponse> call, Response<ItemsListResponse> response) {
                List<Items> itemsFree = response.body().getResults();
                mAdapter = new ItemsAdapter( itemsFree, R.layout.list_items, this );
                recyclerView.setAdapter(mAdapter);

                mAdapter.setOnItemClickListener(new ItemsAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, Items obj, int position) {
                        showCustomDialog(obj);
                    }
                });
//                showCustomDialog(items.get(0));
                Log.d("message",itemsFree.toString());
            }

            private void showCustomDialog(final Items itemsFree) {
                final Dialog dialog = new Dialog(InPause.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
                dialog.setContentView(R.layout.dialog_light);
                dialog.setCancelable(true);

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

                ((TextView) dialog.findViewById(R.id.title)).setText(itemsFree.getName());
                ((TextView) dialog.findViewById(R.id.targetQty)).setText(itemsFree.getTargetQty());
                ((TextView) dialog.findViewById(R.id.completeQty)).setText(itemsFree.getPauseQty());

                ((Button) dialog.findViewById(R.id.dlgCancel)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                ((Button) dialog.findViewById(R.id.dlgDone)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String itemId = itemsFree.getId();
                        final String qtyComplete = ((EditText) dialog.findViewById(R.id.edtQty)).getText().toString().trim();

                        ApiCLient apiCLient = new ApiCLient();
                        ApiInterface apiInterface = apiCLient.getClient().create(ApiInterface.class);

                        apiInterface.pauseItem(campaignId, itemId, qtyComplete)
                                .enqueue(new Callback<ItemUpdateResponse>() {
                                    @Override
                                    public void onResponse(Call<ItemUpdateResponse> call, Response<ItemUpdateResponse> response) {
                                        if (response.body().getSuccess()) {
                                            getFreeItemsList();
                                            dialog.dismiss();
                                        } else {
                                            Toast.makeText(InPause.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ItemUpdateResponse> call, Throwable t) {

                                    }
                                });
                    }
                });

                dialog.show();
                dialog.getWindow().setAttributes(lp);
            }

            @Override
            public void onFailure(Call<ItemsListResponse> call, Throwable t) {

            }
        });
    }
}
