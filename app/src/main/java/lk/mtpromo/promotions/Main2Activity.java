package lk.mtpromo.promotions;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import lk.mtpromo.promotions.Model.ChannelListResponse;
import lk.mtpromo.promotions.Model.Channels;
import lk.mtpromo.promotions.Model.OutletListResponse;
import lk.mtpromo.promotions.Model.Outlets;
import lk.mtpromo.promotions.Model.PromotionListResponse;
import lk.mtpromo.promotions.Model.Promotions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main2Activity extends AppCompatActivity {

    private Float floatEnter;
    private Spinner spinnerPromotion;
    private Spinner spinnerChannel;
    private Spinner spinnerOutlet;
    private int promotionId;
    private int channelId;
    private int outletId;
    private String selectedPromotionId;
    private String selectedChannelId;
    private String selectedOutletId;
    private String lat;
    private String lang;
    private String userId;
    private List<Promotions> promotionsArray;
    private List<Channels> channelsArray;
    private List<Outlets> outletsArray;

    private RelativeLayout profileInformationLayout;

    public static final String SHARED_PREFS = "sharedPrefs";
    public static String USERID = "userid";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        profileInformationLayout = findViewById(R.id.proInfoLayout);

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        userId = sharedPreferences.getString(USERID, "");

        initViews();
//        getPromotions();
//        getChannels();
//        getOutlets();

        //    loadingAndDisplayContent();

        new test().execute(Main2Activity.this);

        // Spinner click listener
        spinnerPromotion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                promotionId = (int) id;
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        spinnerChannel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                channelId = (int) id;
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        spinnerOutlet.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                outletId = (int) id;
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void initViews() {
        spinnerPromotion = findViewById(R.id.spnPromotion);
        spinnerChannel = findViewById(R.id.spnChannel);
        spinnerOutlet = findViewById(R.id.spnOutlet);
    }

    private void loadingAndDisplayContent() {
        final LinearLayout lyt_progress = (LinearLayout) findViewById(R.id.lyt_progress);
        lyt_progress.setVisibility(View.VISIBLE);
        lyt_progress.setAlpha(1.0f);
        profileInformationLayout.setVisibility(View.GONE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ViewAnimation.fadeOut(lyt_progress);
            }
        }, 300);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(Main2Activity.this, "loading", Toast.LENGTH_LONG).show();
                profileInformationLayout.setVisibility(View.VISIBLE);
//                initComponent();
            }
        }, 300 + 400);
    }


    public void onClickEnter(View view) {
        lat = "0.0";
        lang = "0.0";
        selectedPromotionId = promotionsArray.get(promotionId).getId();
        selectedChannelId = channelsArray.get(channelId).getId();
        selectedOutletId = outletsArray.get(outletId).getId();
//        userId = "7";
//        if(!TextUtils.isEmpty(selectedPromotionId) && !TextUtils.isEmpty(selectedChannelId)) {
//            enrollCampaign(lat, lang, selectedPromotionId, selectedChannelId, selectedOutletId, userId);
//        }

        Intent intent = new Intent(Main2Activity.this, Camera.class);
        intent.putExtra("LAT", lat);
        intent.putExtra("LANG", lang);
        intent.putExtra("SELECTEDPROMOTIONID", selectedPromotionId);
        intent.putExtra("SELECTEDCHANNELID", selectedChannelId);
        intent.putExtra("SELECTEDOUTLETID", selectedOutletId);
        intent.putExtra("USERID", userId);
        startActivity(intent);
        finish();
    }

//    private void enrollCampaign(String lat, String lang, String selectedPromotionId, String selectedChannelId, String selectedOutletId, String userId) {
//
//        ApiCLient apiCLient = new ApiCLient();
//        ApiInterface apiInterface = apiCLient.getClient().create(ApiInterface.class);
//
//        apiInterface.enrollCampaign(lat, lang, selectedPromotionId, selectedChannelId, selectedOutletId, userId)
//                .enqueue(new Callback<PromotionInformationResponse>() {
//                    @Override
//                    public void onResponse(Call<PromotionInformationResponse> call, Response<PromotionInformationResponse> response) {
////                 = response.body().getData();
//                        Log.d("response", "res"+response);
////                response.body();
//                        if (response.body().getSuccess()) {
//                            Intent intent = new Intent(Main2Activity.this, Camera.class);
//                            startActivity(intent);
//                            finish();
//                        } else {
//                            Toast.makeText(Main2Activity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<PromotionInformationResponse> call, Throwable t) {
//
//                    }
//                });
//
//    }

    /**
     * get promotions list
     */
    private void getPromotions() {

        ApiCLient apiCLient = new ApiCLient();
        ApiInterface apiInterface = apiCLient.getClient().create(ApiInterface.class);


    }

    /**
     * get channels list
     */
    private void getChannels() {

        ApiCLient apiCLient = new ApiCLient();
        ApiInterface apiInterface = apiCLient.getClient().create(ApiInterface.class);


    }

    /**
     * get outlets list
     */
    private void getOutlets() {


    }

    class test extends AsyncTask<Context, String, String> {
        final LinearLayout lyt_progress = (LinearLayout) findViewById(R.id.lyt_progress);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            lyt_progress.setVisibility(View.VISIBLE);
            lyt_progress.setAlpha(1.0f);
            profileInformationLayout.setVisibility(View.GONE);
        }

        @Override
        protected String doInBackground(Context... contexts) {
            ApiCLient apiCLient = new ApiCLient();
            ApiInterface apiInterface = apiCLient.getClient().create(ApiInterface.class);

            Call<OutletListResponse> call = apiInterface.getOutlets();
            call.enqueue(new Callback<OutletListResponse>() {
                @Override
                public void onResponse(Call<OutletListResponse> call, Response<OutletListResponse> response) {
                    List<Outlets> outlets = response.body().getResults();
                    outletsArray = response.body().getResults();

                    List<String> lables = new ArrayList<String>();

//        txtCategory.setText("");

                    for (int i = 0; i < outlets.size(); i++) {
                        lables.add(outlets.get(i).getName());
                    }

                    // Creating adapter for spinner
                    ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(Main2Activity.this, android.R.layout.simple_spinner_item, lables);

                    // Drop down layout style - list view with radio button
                    spinnerAdapter
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    // attaching data adapter to spinner
                    spinnerOutlet.setAdapter(spinnerAdapter);
                }

                @Override
                public void onFailure(Call<OutletListResponse> call, Throwable t) {

                }
            });

            Call<ChannelListResponse> callChannel = apiInterface.getChannels();
            callChannel.enqueue(new Callback<ChannelListResponse>() {
                @Override
                public void onResponse(Call<ChannelListResponse> call, Response<ChannelListResponse> response) {
                    List<Channels> channels = response.body().getResults();
                    channelsArray = response.body().getResults();

                    List<String> lables = new ArrayList<String>();

//        txtCategory.setText("");

                    for (int i = 0; i < channels.size(); i++) {
                        lables.add(channels.get(i).getName());
                    }

                    // Creating adapter for spinner
                    ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(Main2Activity.this, android.R.layout.simple_spinner_item, lables);

                    // Drop down layout style - list view with radio button
                    spinnerAdapter
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    // attaching data adapter to spinner
                    spinnerChannel.setAdapter(spinnerAdapter);
                }

                @Override
                public void onFailure(Call<ChannelListResponse> call, Throwable t) {

                }
            });

            Call<PromotionListResponse> callPromotions = apiInterface.getPromotions();
            callPromotions.enqueue(new Callback<PromotionListResponse>() {
                @Override
                public void onResponse(Call<PromotionListResponse> call, Response<PromotionListResponse> response) {
                    List<Promotions> promotions = response.body().getResults();
                    promotionsArray = response.body().getResults();

                    List<String> lables = new ArrayList<String>();

//        txtCategory.setText("");

                    for (int i = 0; i < promotions.size(); i++) {
                        lables.add(promotions.get(i).getName());
                    }

                    // Creating adapter for spinner
                    ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(Main2Activity.this, android.R.layout.simple_spinner_item, lables);

                    // Drop down layout style - list view with radio button
                    spinnerAdapter
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    // attaching data adapter to spinner
                    spinnerPromotion.setAdapter(spinnerAdapter);
                }

                @Override
                public void onFailure(Call<PromotionListResponse> call, Throwable t) {

                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ViewAnimation.fadeOut(lyt_progress);
                }
            }, 3000);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    profileInformationLayout.setVisibility(View.VISIBLE);
//                initComponent();
                }
            }, 3000 + 400);
        }
    }

}
