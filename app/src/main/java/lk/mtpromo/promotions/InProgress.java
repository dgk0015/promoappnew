package lk.mtpromo.promotions;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import lk.mtpromo.promotions.Model.CampaignPauseResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InProgress extends AppCompatActivity {

    private Button buttonPause;
    private Button buttonStop;
    private TextView textViewNIC;
    private TextView textViewTransactionId;
    private TextView textViewTime;
    private TextView textViewDate;
    private String campaignId;
    private String nic;
    private String transactionId;

    public static final String SHARED_PREFS = "sharedPrefs";
    public static String NIC = "nic";
    public static String CAMPAIGNID = "campaignId";
    public static String TRANSACTIONID = "tansactionId";
    public static String USERID = "userid";
    private String lat;
    private String lang;

    LocationTrack locationTrack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_progress);

        textViewNIC = findViewById(R.id.txtNIC);
        textViewTransactionId = findViewById(R.id.txtTransactionId);

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        campaignId = sharedPreferences.getString(CAMPAIGNID, "");
        nic = sharedPreferences.getString(NIC, "");
        transactionId = sharedPreferences.getString(TRANSACTIONID, "");
        textViewNIC.setText(nic);
        textViewTransactionId.setText(transactionId);

//        campaignId = getIntent().getExtras().getString("CAMPAIGNID");

        final Date currentTime = Calendar.getInstance().getTime();
//        String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
//        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

//        System.out.println("DateTime" +currentDate);

        locationTrack = new LocationTrack(InProgress.this);

        if (locationTrack.canGetLocation( )) {

            lang = Double.toString(locationTrack.getLongitude());
            lat = Double.toString(locationTrack.getLatitude());

        } else {

            locationTrack.showSettingsAlert();
        }

        String pattern = "yyyy-MM-dd";
        String pattern2 = "hh:mm a";
        String pattern3 = "yyyy-MM-dd hh:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(pattern2);
        SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat(pattern3);
        String date = simpleDateFormat.format(new Date());
        String time = simpleDateFormat2.format(new Date());
        final String datetime = simpleDateFormat3.format(new Date());
        System.out.println("DateTime1" +nic);
        System.out.println("DateTime2" +transactionId);

        textViewTime = findViewById(R.id.txtTime);
        textViewDate = findViewById(R.id.txtDate);
        buttonPause = findViewById(R.id.btnPause);
        buttonStop = findViewById(R.id.btnStop);

        textViewTime.setText(time);
        textViewDate.setText(date);

        buttonPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiCLient apiCLient = new ApiCLient();
                ApiInterface apiInterface = apiCLient.getClient().create(ApiInterface.class);

                apiInterface.pauseCampaign(transactionId, lang, lat, datetime)
                        .enqueue(new Callback<CampaignPauseResponse>() {
                            @Override
                            public void onResponse(Call<CampaignPauseResponse> call, Response<CampaignPauseResponse> response) {
                                if (response.body().getSuccess()) {
                                    Intent intent = new Intent(InProgress.this, InPause.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(InProgress.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<CampaignPauseResponse> call, Throwable t) {

                            }
                        });
            }
        });

        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiCLient apiCLient = new ApiCLient();
                ApiInterface apiInterface = apiCLient.getClient().create(ApiInterface.class);

                apiInterface.endCampaign(transactionId, lang, lat, datetime)
                        .enqueue(new Callback<CampaignPauseResponse>() {
                            @Override
                            public void onResponse(Call<CampaignPauseResponse> call, Response<CampaignPauseResponse> response) {
                                if (response.body().getSuccess()) {
                                    Intent intent = new Intent(InProgress.this, InStop.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(InProgress.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<CampaignPauseResponse> call, Throwable t) {

                            }
                        });
            }
        });
//
//        byte[] byteArray = getIntent().getByteArrayExtra("image");
//        bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
//
//        imgPreview.setImageBitmap(bmp);
    }
}
