package lk.mtpromo.promotions;

import lk.mtpromo.promotions.Model.CampaignPauseResponse;
import lk.mtpromo.promotions.Model.ChannelListResponse;
import lk.mtpromo.promotions.Model.FreeItemsListResponse;
import lk.mtpromo.promotions.Model.ItemUpdateResponse;
import lk.mtpromo.promotions.Model.ItemsListResponse;
import lk.mtpromo.promotions.Model.OutletListResponse;
import lk.mtpromo.promotions.Model.ProfileInformationResponse;
import lk.mtpromo.promotions.Model.PromotionInformationResponse;
import lk.mtpromo.promotions.Model.PromotionListResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiInterface {

    @POST("create/user")
    @FormUrlEncoded
    Call<ProfileInformationResponse> createUser(
            @Field("name") String name,
            @Field("mobile_number") String mobile_number,
            @Field("nic") String nic,
            @Field("device_id") String device_id
    );

    @POST("enroll/campaign")
    @FormUrlEncoded
    Call<PromotionInformationResponse> enrollCampaign(
            @Field("lat") String lat,
            @Field("lang") String lang,
            @Field("promotion_id") String promotion_id,
            @Field("channel_id") String channel_id,
            @Field("outlet_id") String outlet_id,
            @Field("user_id") String user_id,
            @Field("image") String image
    );

    @POST("campaign/{id}/pause/item")
    @FormUrlEncoded
    Call<ItemUpdateResponse> pauseItem(
            @Path("id") String id,
            @Field("item_id") String itemId,
            @Field("pause_qty") String qtyComplete
    );

    @POST("promotion/{id}/pause")
    @FormUrlEncoded
    Call<CampaignPauseResponse> pauseCampaign(
            @Path("id") String id,
            @Field("lat") String lat,
            @Field("lang") String lang,
            @Field("time") String time
    );

    @POST("promotion/{id}/resume")
    @FormUrlEncoded
    Call<CampaignPauseResponse> resumeCampaign(
            @Path("id") String id,
            @Field("lat") String lat,
            @Field("lang") String lang,
            @Field("time") String time
    );

    @POST("promotion/{id}/end")
    @FormUrlEncoded
    Call<CampaignPauseResponse> endCampaign(
            @Path("id") String id,
            @Field("lat") String lat,
            @Field("lang") String lang,
            @Field("time") String time
    );

    @GET("campaign/{id}/general/items")
    Call<ItemsListResponse> getItems(@Path("id") String id);

    @GET("campaign/{id}/free/items")
    Call<ItemsListResponse> getItemsFree(@Path("id") String id);

    @GET("free/items")
    Call<FreeItemsListResponse> getFreeItems();

    @GET("promotion")
    Call<PromotionListResponse> getPromotions();

    @GET("channel")
    Call<ChannelListResponse> getChannels();

    @GET("outlet")
    Call<OutletListResponse> getOutlets();

//    @POST("create/user")
//    Call<ProfileInformation> createUser(@Query("name") String name, @Query("mobile_number") String mobileNumber, @Query("nic") String nic);

}
